package com.example.kafka1.consumer.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Consumer {

    @KafkaListener(topics = "baeldung", groupId = "foo")
    public void consume(String message) {
        log.info("Received Message in group foo: {}", message);
    }

}
