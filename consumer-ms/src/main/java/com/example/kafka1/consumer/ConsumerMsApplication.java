package com.example.kafka1.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class ConsumerMsApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerMsApplication.class, args);
    }
}
