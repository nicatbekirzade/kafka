package com.example.kafka1.consumer.config;

import com.example.kafka1.common.config.KafkaTopicConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import({KafkaTopicConfig.class})
@Configuration
public class CommonConfig {
}
