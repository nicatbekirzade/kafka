package com.example.kafka1.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class ProducerMsApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProducerMsApplication.class, args);
    }
}
